# OpenML dataset: Contaminant-detection-in-packaged-cocoa-hazelnut-spread-jars-using-Microwaves-Sensing-and-Machine-Learning-10.5GHz(Urbinati)

https://www.openml.org/d/45539

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Contaminant-detection-in-packaged-cocoa-hazelnut-spread-jars-using-Microwaves-Sensing-and-Machine-Learning-10.5GHz(Urbinati)
----------------

This dataset is part of a series of five different datasets
each one measured with a different microwave frequency: 9.0, 9.5, 10.0, 10.5, 11.0 GHz.
PAY ATTENTION: THE DATASET PRESENTED IN THIS PAGE HAS BEEN ACQUIRED AT 10.5 GHz!
IF YOU ARE LOOKING FOR THE OTHER FOUR DATASETS, VISIT THE OPENML PROFILE
OF THE AUTHOR OF THIS DATASET.

The following description is valid for all the five datasets.

Dataset description.
To detect contaminants accidentally included in industrial food,
Microwave Sensing (MWS) can be used as a contactless detection method,
in particular when the food is already packaged.

MWS uses microwaves to illuminate the target object through a set of antennas,
records the scattered waves, and uses Machine Learning to predict
the presence of contaminants inside the target object.

In this application the target object is a cocoa-hazelnut spread jar
and each instance (sample) of this dataset consists in 30 scattering parameters
of the network composed by: antennas, target object (a jar
w/ or w/o a contaminant inside) and medium (i.e. the air) in between.
The types of contaminants vary from metal to glass and plastic.
Each sample has been measured at five different microwave frequencies
that are: 9.0, 9.5, 10.0, 10.5, 11.0 GHz.
PAY ATTENTION: THE DATASET PRESENTED IN THIS PAGE HAS BEEN ACQUIRED AT 10.5 GHz!
IF YOU ARE LOOKING FOR THE OTHER FOUR DATASETS, VISIT THE OPENML PROFILE
OF THE AUTHOR OF THIS DATASET.

**Data Set Characteristics:**

  :Microwave frequency used for acquisition: 10.5 GHz

  :Total Number of Instances: 2400

  :Total Number of Uncontaminated Instances: 1200

  :Total Number of Contaminated Instances: 1200

  :Total Number of Classes: 11

  :Target: The last column, column 31, contains the class label as integer value

  :Number of Contaminated Instances Divided Per Class (full explanation and
  pictures in [2]):

      - "air_surface" (i.e. cap-shape plastic with the same dielectric constant of the air): 200

      - "big_pink_plastic_shere_middle": 100

      - "big_pink_plastic_shere_surface": 100

      - "glass_fragment_middle": 100

      - "glass_fragment_surface": 100

      - "small_metal_sphere_middle": 100

      - "small_metal_sphere_surface": 100

      - "small_plastic_sphere_middle": 100

      - "small_plastic_sphere_surface": 100

      - "small_triangular_plastic_fragment_surface": 200

  "surface" means that the instance was placed on top of the cocoa-hazelnut spread,
   at the spread-air interface

  "middle" means that the instance was placed in the middle of the jar filled with
   cocoa-hazelnut spread


  :Number of Attributes in a generic Instance: 30


  :Attribute Information: This is the 6x6 Scattering Matrix (S):

  S = [        s12, s13, s14, s15, s16,  
        s21,        s23, s24, s25, s26,  
        s31, s32,        s34, s35, s36,  
        s41, s42, s43,        s45, s46,  
        s51, s52, s53, s54,        s56,  
        s61, s62, s63, s64, s65,        ]

  The first 30 attributes (columns) of an instance are the 15 elements of the triangular
  upper part of S. Since each of these elements is a complex number with real and
  imaginary parts, each instance is a vector of 15x2=30 attributes. The real (real) and
  imaginary (img) parts of each element are placed one after the other.
  The scattering parameters are ordered row by row from left to right, e.g.:
  s12_real, s12_img, s13_real, s13_img, ..., s16_real, s16_img, s21_real, s21_img, ... .
  The self-scattering parameters, i.e. those placed on the main diagional of S,
  are not part of the dataset, as exaplined in [1].


Note: This dataset is realeased without pre-processing.

For more information read the reference papers:

[1] L. Urbinati, M. Ricci, G. Turvani, J. A. T. Vasquez, F. Vipiana and M. R. Casu, "A Machine-Learning Based Microwave Sensing Approach to Food Contaminant Detection," 2020 IEEE International Symposium on Circuits and Systems (ISCAS), Seville, Spain, 2020, pp. 1-5, doi: 10.1109/ISCAS45731.2020.9181293
Link to the paper: https://ieeexplore.ieee.org/abstract/document/9181293

[2] M. Ricci, B. Stitic, L. Urbinati, G. D. Guglielmo, J. A. T. Vasquez, L. P. Carloni, F. Vipiana, and M. R. Casu, "Machine-learning-based microwave sensing: A case study for the food industry," IEEE Journal on Emerging and Selected Topics in Circuits and Systems, vol. 11, no. 3, pp. 503-514, 2021
Link to the paper: https://ieeexplore.ieee.org/abstract/document/9489295

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45539) of an [OpenML dataset](https://www.openml.org/d/45539). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45539/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45539/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45539/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

